import os
import sys

sys.path.insert(0, '/var/www/sites/site_thumbnail')
sys.path.insert(1, '/var/www/sites/site_thumbnail/venv/lib/python2.7')
sys.path.insert(2, '/var/www/sites/site_thumbnail/venv/lib/python2.7/site-packages')

os.environ['DJANGO_SETTINGS_MODULE'] = 'site_thumbnail.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
