import json

from rest_framework import authentication
from rest_framework import exceptions
from django.conf import settings
MAX_COUNT = settings.MAXIMUM_COUNT
from imageapp.models import CreateApiKey, UserProfileDetail, Plan, AnonUsage
from .utils import client_ipaddress,json_error



class Api_KeyAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        apikey = request.META.get('HTTP_AUTHORIZATION')
        if not apikey:
             apikey = request.query_params.get("apikey") or request.data.get("apikey")
        if not apikey:
            if request.method not in ["POST", "PUT"]:
                raise exceptions.AuthenticationFailed("API Key is missing")
            ip_address = client_ipaddress(request)
            try:
                usage, created = AnonUsage.objects.get_or_create(ip_address=ip_address)
                if usage.use_count <= MAX_COUNT:
                    request.usage = usage
                    return self.request.user
                else:
                    raise exceptions.AuthenticationFailed("MAX_COUNT has expired")
            except AnonUsage.DoesNotExist:
                raise exceptions.AuthenticationFailed('Invalid ip_address!')
               
        try:
            api = CreateApiKey.objects.get(apikey=apikey, enabled=True)
        except CreateApiKey.DoesNotExist:
            raise exceptions.AuthenticationFailed('Invalid API key!')
        else:
            user_profile = UserProfileDetail.objects.get(user=api.user)
            try:
                if user_profile.plan:
                    plan= Plan.objects.get(name=user_profile.plan)
                    return (api.user,'')
                return json_error("User has no plan!")
            except UserProfileDetail.DoesNotExist:
                return json_error("plan is not applicable")
