from imageapp.models import ScreenImage
from rest_framework import serializers


class ScreenImageSerializer(serializers.ModelSerializer):
    url = serializers.URLField()

    class Meta:
        model = ScreenImage
        fields=('url', 'width', 'height')


class ImageSerializer(serializers.ModelSerializer):
    requested_url = serializers.ReadOnlyField(source='url')
    screenshot_url = serializers.ReadOnlyField(source='response_url')
    
    class Meta:
        model = ScreenImage
        fields = ('requested_url', 'screenshot_url', 'screen_key')