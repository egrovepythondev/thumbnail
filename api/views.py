import os
from imageapp.models import ScreenImage, UserProfileDetail,CreateApiKey
import json
from imageapp.models import ScreenImage, UserProfileDetail
from api.serializers import ScreenImageSerializer, ImageSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets
from .utils import imagepath,json_error
from .utils import imagepath, json_error
from django.http import HttpResponse


class TaskList(viewsets.ModelViewSet):
    """
    List all users,create,update,delete a new user.
    """
    queryset = ScreenImage.objects.all()
    serializer_class = ImageSerializer


    def get_object(self, screen_key):
        try:
            screen_key = self.kwargs.get("screen_key")
            serializer = ImageSerializer(screen_key)
        except ScreenImage.DoesNotExist:
            return json_error(404, "ScreenImage is not available")
        return Response(serializer.data)


    def get_image(self, **kwargs):
        screen_key = kwargs.get("screen_key")
        if screen_key:
            try:
                img = ScreenImage.objects.get(screen_key=screen_key)
            except ScreenImage.DoesNotExist:
                return json_error(404, "ScreenImage is not available")
            else:
                serializer = ImageSerializer(img)
        else:
            images = ScreenImage.objects.all()
            serializer = ImageSerializer(images, many=True)
        return Response(serializer.data)

        
    def create(self, request):
            serializer = ScreenImageSerializer(data=request.data)
            if serializer.is_valid():

                url = request.data.get("url")
                width = request.data.get('width')
                height = request.data.get('height')

                response = imagepath(url, width, height)
                screen, created= ScreenImage.objects.get_or_create(url=url)
                screen.response_url = response
                screen.save()
                if request.user.is_authenticated():
                    user_profile = UserProfileDetail.objects.get(user=request.user)
                    user_profile.screenimage.add(screen)

                    if user_profile.plan:
                        if user_profile.consumed_request <= user_profile.plan.plan_count:
                            user_profile.consumed_request += 1
                            serializer = ImageSerializer(screen)
                            user_profile.save()
                            return Response(serializer.data, status=status.HTTP_201_CREATED)
                        else:
                            data={'error':'plan is expired'}
                            return Response(data)
                else:
                    request.usage.use_count += 1
                    request.usage.save()
                    serializer = ImageSerializer(screen)
                    return Response(serializer.data, status=status.HTTP_201_CREATED)

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def put_image(self, request, **kwargs):
        screen_key = kwargs.get("screen_key")
        try:
            image = ScreenImage.objects.get(screen_key=screen_key)
        except ScreenImage.DoesNotExist:
            return json_error(404, "ScreenImage is not available")
        serializer = ImageSerializer(image, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    
    def delete_image(self, **kwargs):
        screen_key = kwargs.get("screen_key")
        try:
            img = ScreenImage.objects.get(screen_key=screen_key)
            img.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except ScreenImage.DoesNotExist:
            return json_error(404, "ScreenImage is not available")
