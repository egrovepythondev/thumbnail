from django.conf.urls import include, url
from django.contrib import admin
from api.views import TaskList

admin.autodiscover()

urd_image = TaskList.as_view({
    'put': 'put_image',
    'get': 'get_image',
    'delete':'delete_image'
})

c_image = TaskList.as_view({
    'post': 'create',
    'get': 'get_image',
})


urlpatterns = [
    
    url(r'^admin/', include(admin.site.urls)),
    url(r'^image/$', c_image, name="image"),
    url(r'^image/(?P<screen_key>\w+)/$', urd_image, name="image"),
]