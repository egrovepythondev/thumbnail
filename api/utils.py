import md5
import sys
import os
import json

from django.conf import settings
from django.http import HttpResponse
from PIL import Image
from imageapp.models import ScreenImage

THUMBS_DIR = settings.THUMBS_DIR
WEBIMAGE = settings.WEBIMAGE

def json_error(status, content='-'):
    response = HttpResponse(json.dumps({'detail': content}), status=status)
    response['Content-Type'] = 'application/json'
    return response


def imagepath(url, width, height):
    
    hash = md5.new(url).hexdigest()
    path = THUMBS_DIR + hash
    img_url = "{}.jpg".format(path)
        
    screenimage, created = ScreenImage.objects.get_or_create(url=url)
    screenimage.original_image = "/static/image/original/" + hash + ".jpg"
    screenimage.width = width
    screenimage.height = height
    screenimage.save()

    if not os.path.isfile(path):
        try:
            command = 'xvfb-run --auto-servernum --server-num=1 %swebimage %s %s'%(WEBIMAGE, url, path)
            print "command =", command
            os.system(command)
        except Exception as e:
            message = "Error occured in thumbnail Generating .."
            print str(e), "Exception in OS System"

    response_data = {}
    if img_url:
        response_data['image_url'] = img_url

    outfile = THUMBS_DIR + hash + ".jpg"
    size = (int(width), int(height))
    try:
        image = resizeimage(img_url, outfile, size)
    except:
        screenshot = settings.SITE_URL + outfile.split('imageapp')[1]
        return screenshot
    else:
        screenimage = settings.SITE_URL + image.split('imageapp')[1]
        return screenimage

def resizeimage(img_url, outfile, size):
    im = Image.open(img_url)
    imageresize = im.resize(size, Image.ANTIALIAS)
    imageresize.save(outfile, 'JPEG', quality=75)
    return outfile

if __name__ == '__main__':
    try:
        arg = sys.argv[1]
    except IndexError:
        arg = None
    return_outfile = imagepath(arg)
    

def client_ipaddress(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip




