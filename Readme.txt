**STEPS**

1.Clone the project: 

git clone https://egrovepythondev@bitbucket.org/egrovepythondev/thumbnail.git

2.Create virtual env and install the requirements of project
    * pip install -r requirements.txt
3.Create db in name **thumbnaildb** using postgres
4.Make migrations and update in db using command
    
    *python manage.py makemigrations app-name
    *python manage.py migrate

5.Install the dependencies mentioned in readme.txt file
6.Run the server using command
  
  * python manage.py runserver
        
 
-------------------------------------------------------------------------------

**readme.txt:**

Install And Run the Thumbnail Generator
# TODO: not in use anymore
1) sudo apt-get install subversion libqt4-webkit libqt4-dev g++
2) svn co https://cutycapt.svn.sourceforge.net/svnroot/cutycapt
3) cd cutycapt/CutyCapt
4) qmake
5) make
./CutyCapt --url=http://lapin-blanc.net --out=example.png
--------------

Updated Requirments
http://screenshots.postbit.com/how-to-install-webimage-site-screen-capture.html




1) subprocess
2) md5
3) sorl-thumbnail