from django.contrib import admin
from imageapp.models import ScreenImage
from imageapp.models import CreateApiKey
from imageapp.models import UserProfileDetail
from imageapp.models import Plan
from imageapp.models import AnonUsage

admin.site.register(ScreenImage)
admin.site.register(CreateApiKey)
admin.site.register(UserProfileDetail)
admin.site.register(Plan)
admin.site.register(AnonUsage)