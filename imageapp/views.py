import os
import hashlib
import json
import stripe

from django.shortcuts import render, render_to_response
from django.conf import settings
from PIL import Image
from .forms import ViewImage
from .models import ScreenImage,Plan,UserProfileDetail
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django.contrib.auth import authenticate, login,logout
from django.http import HttpResponse, HttpResponseRedirect

CUTYCAPT = settings.CUTY_CAPT
THUMBS_DIR = settings.THUMBS_DIR
WEBIMAGE = settings.WEBIMAGE


class HomePageView(TemplateView):
    template_name = "imageapp/home.html"


def home_page(request):

     if request.user.is_authenticated():
         user = request.user
     return render_to_response('imageapp/home.html', {})


@csrf_exempt
def login_user(request):

    state = "Please log in below..."
    username = password = ''

    if request.method=="POST":

        username = json.loads(request.body).get('username')
        password = json.loads(request.body).get('password')
        try:
            user = authenticate(username=username, password=password)
            login(request, user)
            print "Success"
            data = {'status':'success'}
            return HttpResponse(json.dumps(data), content_type="application/json")
        except Exception as exp:
            data = {'status':'failure'}
            return HttpResponse(json.dumps(data), content_type="application/json")

    user = request.user
    return render_to_response('imageapp/login.html',{'state':state, 'username': user})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login/')



@csrf_exempt
def index(request):
    if request.method == 'POST':
        img = ViewImage(request.POST)

        if img.is_valid():
            url = request.POST.get("url")
            width = request.POST.get('width')
            height = request.POST.get('height')

            hash = hashlib.new(url).hexdigest()
            path = THUMBS_DIR + hash
            img_url = "{}.jpg".format(path)

            screenimage, created = ScreenImage.objects.get_or_create(url=url)
            screenimage.original_image = "/static/image/original/" + hash + ".jpg"
            screenimage.width = width
            screenimage.height = height
            screenimage.save()

            if not os.path.isfile(path):
                try:
                    command = 'xvfb-run --auto-servernum --server-num=1 %swebimage %s %s'%(WEBIMAGE, url, path)
                    print "command =", command
                    os.system(command)
                except Exception as e:
                    message = "Error occured in thumbnail Generating .."
                    print str(e), "Exception in OS System"

            response_data = {}
            if img_url:
                response_data['image_url'] = img_url

            outfile = THUMBS_DIR + hash + ".jpg"
            size = (int(width), int(height))

            try:
                resizeimage(img_url, outfile, size)
            except:
                screenshot = settings.SITE_URL + img_url.split('imageapp')[1]
                return screenshot
            else:
                screenimage = settings.SITE_URL + outfile.split('imageapp')[1]
                return render(request, 'imageapp/image.html', {'screenimage' : screenimage})
    else:
        img = ViewImage()

    return render(request, 'imageapp/index.html', {'img': img })


def resizeimage(img_url, outfile, size):
    im = Image.open(img_url)
    imageresize = im.resize(size, Image.ANTIALIAS)
    imageresize.save(outfile, 'JPEG', quality=75)
    return outfile


@csrf_exempt
def SubscribeView(request):
    stripe.api_key = settings.STRIPE_SECRET_KEY
    response_data = {"state": "fail", "plan": None}
    assert request.method == 'POST', request.method
    user = request.user

    token_id = request.POST['token[id]']
    userprofiledetail = UserProfileDetail.objects.get(user=request.user)
    plan = Plan.objects.get(name=request.POST["plan"])

    stripe_customer_id = userprofiledetail.stripe_customer_id
    stripe_subscription_id = userprofiledetail.stripe_subscription_id

    try:
        customer = stripe.Customer.retrieve(stripe_customer_id)
        subscription = customer.subscriptions.retrieve(stripe_subscription_id)
    except stripe.error.InvalidRequestError:
        metadata = {
            'user_id': user.id,
        }
        try:
            customer = stripe.Customer.create(
                email=user.email,
                metadata=metadata,
                plan=plan.name.lower(),
                card=token_id)
        except stripe.error.InvalidRequestError, exp:
            print "Exception when creating Customer, ", str(exp)
            response_data["state"] = "no_plan"
            response_data["plan"] = plan.name
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            try:
                # TODO on live deployment put "True"
                assert not customer.livemode, customer
            except AssertionError:
                response_data["state"] = "test"
                return HttpResponse(json.dumps(response_data),
                                    content_type="application/json")
            else:
                userprofiledetail.stripe_customer_id = customer.id
                userprofiledetail.stripe_subscription_id = customer.subscriptions.data[0].id
                userprofiledetail.pricing_plan = plan
                userprofiledetail.save()

    else:
        customer.card = token_id
        try:
            subscription.plan = plan.name.lower()
            subscription.save()
        except stripe.error.StripeError, exp:
            print "Error while modifying plan , ", exp
            response_data["state"] = "no_plan"
            response_data["plan"] = plan.name.lower()
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")

        userprofiledetail.plan = plan
        customer.save()
        userprofiledetail.save()
    return HttpResponse("success")


def plan_details(request):
    plan_list = Plan.objects.all()
    return render_to_response("imageapp/plan.html", {'plan_list': plan_list})