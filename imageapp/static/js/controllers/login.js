var app = angular.module("loginapp", ['ngToast']).config(function($httpProvider, $interpolateProvider,ngToastProvider) {
                    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
                    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
                    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';
                    $interpolateProvider.startSymbol('[[');
                    $interpolateProvider.endSymbol(']]');
                    ngToastProvider.configure({
                            animation: 'slide',
                            additionalClasses: 'my-animation',
                            horizontalPosition:'center',
                            verticalPosition:'top'

                        });
                    });

                    app.controller('loginController', function ($scope,$http,ngToast) {

                        $scope.loginUser = function () {

                            var data= {

                                username: $scope.username,
                                password: $scope.password,
                            }
                            console.log(data);

                            var urls='/home';
                        $http.post(urls, data)

                                .success(function (data, status) {
                                     ngToast.create({
                                            className:'success',
                                            content: '<div>\
						                             '+ data.username+'</div>'


                                       });

                                })
                                .error(function (data, status) {
                                            ngToast.create({
                                            className:'warning',
                                            content: '<div>\
						                             The username and passowrd is wrong!!</div>'
                                           });

						         });
                          }
                        ngToast.dismiss();

               });
//ngToast.dismiss();
