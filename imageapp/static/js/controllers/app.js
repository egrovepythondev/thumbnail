var app = angular.module("myapp", ['ngToast']).config(function($httpProvider, $interpolateProvider,ngToastProvider) {
                    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
                    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
                    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';
                    $interpolateProvider.startSymbol('[[');
                    $interpolateProvider.endSymbol(']]');
                    ngToastProvider.configure({
                            animation: 'slide',
                            additionalClasses: 'my-animation',
                            horizontalPosition:'center',
                            verticalPosition:'top'

                        });
                    });

                    app.controller('dataController', function ($scope,$http,ngToast) {

                        $scope.availableOptions = [
                                {id: 1, value: 200},
                                {id: 2, value: 300},
                                {id: 3, value: 600},
                                {id: 4, value: 800},
                                {id: 5, value: 1024}
                                 ];
                        $scope.success_message = false;
                        $scope.error_message = false;
                        $scope.SendHttpPostData = function () {

                            var data= {

                                url: $scope.url,
                                width: $scope.width,
                                height:$scope.height,
                            }

                            if ($scope.apikey!=undefined){
                                data["apikey"]=$scope.apikey
                            }
                             $scope.loading=true;
                        $http({
                            url: '/api/image/',
                            method: 'POST',
                            data: data,
                        }).success(function (data, status) {
                                       $scope.loading=false;
                                       $scope.ServerResponse=data.screenshot_url;
                                       $scope.error_message = false;
                                         if (data.error) {
                                            $scope.success_message = false;
                                            ngToast.create({
                                                className: 'danger',
                                                content: '<div>\
                                                             ' + data.error + '</div>'
                                            })
                                        }
                                        else {
                                            $scope.success_message = true;
                                            ngToast.create({
                                                className: 'success',
                                                content: '<div>\
                                                             ' + data.screenshot_url + '</div>'
                                            })
                                        }
                                })
                               .error(function (data, status) {
                                     $scope.loading=false;
                                     $scope.ServerResponses =data.url;
                                     ngToast.create({
                                            className:'warning',
                                            content: '<div>\
						                             '+ data.url+'</div>'
                                                  });
                                    $scope.success_message = false;
                                    $scope.error_message = true;
						         });
                        }
                        ngToast.dismiss();
               });
app.controller('loginController', [ '$scope', '$http', '$window', function ( $scope, $http, $window) {

    $scope.check = function () {
        var data = {

            username: $scope.username,
            password: $scope.password,
        }


        var urls = '/login/';
        $http.post(urls, data)

            .success(function (data,status) {

                if (data.status=='success') {

                    $window.location.href = '/';


                }
                else {
                    $window.location.href = "/login/";

                }


            })
    }
}]);
