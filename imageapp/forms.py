from django.forms import ModelForm
from .models import ScreenImage
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator

class ViewImage(ModelForm):
    class Meta:
        model = ScreenImage
        fields = ['url', 'width', 'height']
    
    def clean_url(self):
        url = self.cleaned_data["url"]
        if not url:
            raise ValidationError("Url field is required!")
        val = URLValidator()
        try:
            val(url)
        except ValidationError:
            raise ValidationError("Please provide a valid url!")
        else:
            return url