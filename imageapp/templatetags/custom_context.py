from imageapp.models import CreateApiKey, newtoken
from django import template

register = template.Library()


@register.filter(name="get_apikey")
def get_api(user):
        if user.is_authenticated():
            api, created = CreateApiKey.objects.get_or_create(user=user)
            if created:
                api.apikey = newtoken()
                api.save()
            return api.apikey
            return user

        return ""
