from imageapp.models import CreateApiKey, newtoken
from django.shortcuts import redirect


def get_api(request):

        if request.user.is_authenticated():
            api, created = CreateApiKey.objects.get_or_create(user=request.user)
            if created:
                api.apikey = newtoken()
                api.save()
            return api.apikey
        return redirect('imageapp/login.html')
