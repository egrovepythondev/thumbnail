import uuid
import datetime

from decimal import Decimal
from django.db import models
from django.contrib.auth.models import User
from imageapp.choices import VALUES

def newtoken():
    """
    return hexa digits
    """
    return uuid.uuid4().hex


class Plan(models.Model):
    name = models.CharField(max_length=1000, unique=True)
    price = models.DecimalField(max_digits=5, decimal_places=2,default=Decimal(0.0))
    plan_count = models.IntegerField(default=0)

    def __unicode__(self):
        return "%s" % self.name


class CreateApiKey(models.Model):
    user = models.OneToOneField(User, related_name='api_key')
    apikey = models.CharField(max_length=1000, default=newtoken, unique=True)
    created = models.DateTimeField(default=datetime.datetime.now)
    enabled = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s ApiKey %s for %s" %(self.user, self.apikey, self.enabled)


class ScreenImage(models.Model):
    url = models.CharField(max_length=1000, unique=True)
    response_url = models.CharField(max_length=1000, blank=True, null=True)
    screen_key = models.CharField(max_length=1000, default=newtoken, unique=True)
    width = models.IntegerField(choices=VALUES, default=600)
    height = models.IntegerField(choices=VALUES, default=600)
    original_image = models.ImageField(default = 'static/image/original/')

    def __unicode__(self):
        return "%s" % self.url


class UserProfileDetail(models.Model):
    user = models.OneToOneField(User)
    screenimage = models.ManyToManyField(ScreenImage, related_name='screenimages',blank=True, null=True)
    stripe_customer_id = models.CharField(max_length=80, null=True, blank=True)
    stripe_subscription_id = models.CharField(max_length=80, null=True, blank=True)
    plan = models.ForeignKey(Plan, blank=True, null=True)
    consumed_request = models.IntegerField(default=0)
    
    def __unicode__(self):
        return "%s" % self.user
    
class AnonUsage(models.Model):
    ip_address = models.CharField(max_length=15, unique=True)
    use_count = models.IntegerField(default=0)
    
    def __unicode__(self):
        return "%s" % self.ip_address
    