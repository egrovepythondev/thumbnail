from django.conf.urls import url
from django.views.generic import TemplateView
from imageapp.views import HomePageView

urlpatterns = [

     url(r'^plans/$', 'imageapp.views.plan_details', name="plan" ),
     url(r'^login/$','imageapp.views.login_user', name='login'),
     url(r'^subscribe_plan/$', 'imageapp.views.SubscribeView', name='subscribe'),
     url(r'^logout/$','imageapp.views.logout_view', name='logout'),
     url(r'^$', HomePageView.as_view()),
]


