# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from decimal import Decimal
import imageapp.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AnonUsage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip_address', models.CharField(unique=True, max_length=15)),
                ('use_count', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='CreateApiKey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('apikey', models.CharField(default=imageapp.models.newtoken, unique=True, max_length=1000)),
                ('created', models.DateTimeField(default=datetime.datetime.now)),
                ('enabled', models.BooleanField(default=True)),
                ('user', models.OneToOneField(related_name='api_key', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=1000)),
                ('price', models.DecimalField(default=Decimal('0'), max_digits=5, decimal_places=2)),
                ('plan_count', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='ScreenImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(unique=True, max_length=1000)),
                ('response_url', models.CharField(max_length=1000, null=True, blank=True)),
                ('screen_key', models.CharField(default=imageapp.models.newtoken, unique=True, max_length=1000)),
                ('width', models.IntegerField(default=600, choices=[(200, 200), (300, 300), (600, 600), (800, 800), (1024, 1024)])),
                ('height', models.IntegerField(default=600, choices=[(200, 200), (300, 300), (600, 600), (800, 800), (1024, 1024)])),
                ('original_image', models.ImageField(default=b'static/image/original/', upload_to=b'')),
            ],
        ),
        migrations.CreateModel(
            name='UserProfileDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('stripe_customer_id', models.CharField(max_length=80, null=True, blank=True)),
                ('stripe_subscription_id', models.CharField(max_length=80, null=True, blank=True)),
                ('consumed_request', models.IntegerField(default=0)),
                ('plan', models.ForeignKey(blank=True, to='imageapp.Plan', null=True)),
                ('screenimage', models.ManyToManyField(related_name='screenimages', null=True, to='imageapp.ScreenImage', blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
