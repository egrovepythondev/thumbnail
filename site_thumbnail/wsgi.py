"""
WSGI config for site_thumbnail project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os
import sys



sys.path.insert(0, '/var/www/sites/web2image/thumbnail')
sys.path.insert(1, '/var/www/sites/web2image/thumbnail/site_thumbnail')
sys.path.insert(2, '/var/www/sites/web2image/venv')
sys.path.insert(3, '/var/www/sites/web2image/venv/lib/python2.7/site-packages')

os.environ['DJANGO_SETTINGS_MODULE'] = "site_thumbnail.settings"

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
