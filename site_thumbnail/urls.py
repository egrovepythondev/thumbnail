from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [

    url(r'^', include('imageapp.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include('api.urls')),
    # url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
]